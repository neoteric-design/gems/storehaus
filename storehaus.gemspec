$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "storehaus/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "storehaus"
  s.version     = Storehaus::VERSION
  s.authors     = ["Madeline Cowie"]
  s.email       = ["madeline@cowie.me"]
  s.homepage    = "https://www.neotericdesign.com"
  s.summary     = "Extend ActiveStorage with organization and editing tools"
  s.description = "Extend ActiveStorage with organization and editing tools"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "Rakefile", "README.md"]

  s.add_dependency "rails", ">= 6.0"
  s.add_dependency 'sass-rails', '>= 6'
  s.add_dependency 'nokogiri'
  s.add_dependency 'kaminari'

  s.add_development_dependency "pg"

  s.add_development_dependency 'capybara'
  s.add_development_dependency 'factory_bot_rails', '>= 4.10.0'
  s.add_development_dependency 'image_processing'
  s.add_development_dependency 'rspec-rails', '>= 4.0'
  s.add_development_dependency 'selenium-webdriver'
  s.add_development_dependency 'simplecov'
end
