# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

# Storehaus

Extend ActiveStorage with organization and editing tools.

Storehaus adds several features on top of Rails' ActiveStorage to create a rich file library for a CMS or other applications.

### Library Metadata

Tag and organize files with a set of configurable metadata fields.

### File Browser

Browse a configurable and searchable list of files.

### Image cropper



## Usage

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'storehaus'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install storehaus
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
