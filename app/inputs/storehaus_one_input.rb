class StorehausOneInput
  include Formtastic::Inputs::Base

  delegate :concat, to: :template, prefix: false

  def to_html
    input_wrapping do
      label_html <<
        template.content_tag(:div, class: 'storehaus-widget') do
          concat thumbnail
          concat cropper_link
          concat metadata_link
          concat remove_link
          concat fields_html
        end
    end
  end

  def thumbnail
    template.link_to picker_path, class: 'storehaus-picker' do
      template.image_tag thumbnail_source(attachment, **thumbnail_options),
                         thumbnail_html_options.merge(class: 'storehaus-thumbnail')
    end
  end

  def cropper_link
    template.link_to cropIcon, cropper_path, class: 'storehaus-action crop',
                                             style: "visibility: #{attachment.attached? ? 'visible' : 'hidden'}"
  end

  def metadata_link
    # template.link_to detailsIcon, metadata_path,
    #                  class: "storehaus-action details", target: "_blank",
    #                   style: "visibility: #{ attachment.attached? ? 'visible' : 'hidden' }"
  end

  def remove_link
    template.link_to removeIcon, '#',
                     class: 'storehaus-action remove',
                     style: "visibility: #{attachment.attached? ? 'visible' : 'hidden'}"
  end

  def fields_html
    template.capture do
      builder.fields_for("#{method}_attachment", attachment_record) do |attachment_fields|
        concat attachment_fields.hidden_field(:_destroy, class: 'storehaus-field destroy')
        concat attachment_fields.hidden_field(:signed_id, class: 'storehaus-field blob-id')
        concat attachment_fields.hidden_field(:id, class: 'storehaus-field attachment-id')
        concat attachment_fields.hidden_field(:storehaus_transformation, class: 'storehaus-field transformation')
      end
    end
  end

  private

  def thumbnail_options
    options[:thumbnail_html] || {}
  end

  def thumbnail_html_options
    options[:thumbnail] || {}
  end

  def attachment
    object.send(method)
  end

  def attachment_record
    object.send("#{method}_attachment") || object.send("build_#{method}_attachment")
  end

  def thumbnail_source(attachment, **options)
    return placeholder_icon unless attachment.attached?

    options[:resize] ||= '300x300>'
    if attachment.image?
      template.rails_representation_path(attachment.storehaus_variant(options))
    elsif attachment.previewable?
      template.rails_representation_path(attachment.preview(options))
    else
      no_preview_icon
    end
  end

  def placeholder_icon
    template.image_path 'storehaus/placeholder.png'
  end

  def missing_icon
    template.image_path 'storehaus/missing.png'
  end

  def no_preview_icon
    template.image_path 'storehaus/no_preview.png'
  end

  def cropIcon
    '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M22 18v-2H8V4h2L7 1 4 4h2v2H2v2h4v8c0 1.1.9 2 2 2h8v2h-2l3 3 3-3h-2v-2h4zM10 8h6v6h2V8c0-1.1-.9-2-2-2h-6v2z"/></svg>'.html_safe
  end

  def detailsIcon
    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 125" enable-background="new 0 0 100 100" xml:space="preserve"><g><path d="M72,92H56.505h-1.009H9V8h63v38.458l7-12.543V8.158C79,4.195,76.23,1,72.267,1H8.708C4.746,1,2,4.195,2,8.158v84.121   C2,96.241,4.746,99,8.708,99h63.559C76.23,99,79,96.241,79,92.279V69.233l-7,12.543V92z"/><rect x="15" y="19" width="39" height="5"/><rect x="15" y="42" width="39" height="5"/><rect x="15" y="31" width="51" height="5"/><rect x="15" y="54" width="51" height="5"/><rect x="15" y="65" width="39" height="5"/><polygon points="89.696,26.201 87.8,29.502 86.982,30.927 94.01,34.963 94.828,33.539 96.724,30.237  "/><polygon points="60,76.589 60,85 67.224,80.626 92.103,37.575 85.075,33.539  "/><circle cx="19.604" cy="81.623" r="4.856"/><circle cx="35.01" cy="81.683" r="4.856"/></g></svg>'.html_safe
  end

  def removeIcon
    '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zm2.46-7.12l1.41-1.41L12 12.59l2.12-2.12 1.41 1.41L13.41 14l2.12 2.12-1.41 1.41L12 15.41l-2.12 2.12-1.41-1.41L10.59 14l-2.13-2.12zM15.5 4l-1-1h-5l-1 1H5v2h14V4z"/><path fill="none" d="M0 0h24v24H0z"/></svg>'.html_safe
  end

  def cropper_path
    if attachment.attached?
      blob_id =  attachment.signed_id
      transform_params = attachment.storehaus_transformation.to_h
    else
      blob_id = 'none'
      transform_params = {}
    end

    params = {}
    params[:ratio] = options[:ratio] if options[:ratio]
    params.merge(transform_params)

    storehaus_routes.cropper_path(blob_id, params)
  end

  def metadata_path
    return '#' unless attachment.attached?

    storehaus_routes.item_path(attachment.storehaus_item)
  end

  def picker_path
    storehaus_routes.picker_path
  end

  def storehaus_routes
    Storehaus::Engine.routes.url_helpers
  end
end
