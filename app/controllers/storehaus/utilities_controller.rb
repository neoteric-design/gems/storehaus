require_dependency "storehaus/application_controller"

module Storehaus
  class UtilitiesController < ApplicationController
    layout 'storehaus/utility'

    def cropper
      find_blob
      build_transformation
    end

    def deliver
      find_blob
      build_transformation
    end

    def transformation_params
      params.slice(:crop).permit(crop: %i[x y w h]).to_h
    end
    helper_method :transformation_params

    private
    def find_blob
      @blob ||= ActiveStorage::Blob.find_signed(params[:id], purpose: :blob_id)
    end

    def build_transformation
      @transformation = Transformation.new(transformation_params)
    end
  end
end
