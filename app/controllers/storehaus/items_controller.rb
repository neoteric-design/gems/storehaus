require_dependency "storehaus/application_controller"

module Storehaus
  class ItemsController < ApplicationController
    def index
      @items = filtered_collection.page(params[:page])
    end

    def show
      @item = scoped_collection.find(params[:id])
    end

    def new
      @item = scoped_collection.new
    end

    def edit
      @item = scoped_collection.find(params[:id])
    end

    def create
      @item = scoped_collection.new(permitted_params)

      respond_to do |format|
        if @item.save
          format.html { redirect_to(action: :edit, id: @item) }
          format.js { js_redirect_to redirect_to(action: :edit, id: @item) }
        else
          format.html { render action: "new" }
          format.js { render js: "alert('Please try again');" }
        end
      end
    end

    def update
      @item = scoped_collection.find(params[:id])
      @item.assign_attributes(permitted_params)

      respond_to do |format|
        if @item.save
          format.html { redirect_to action: :show }
          format.js do
            if params[:save_and_return]
              render template: "storehaus/utilities/deliver", assigns: { blob: @item.blob, transformation: Transformation.new }
            else
              js_redirect_to(new_item_path)
            end
          end
        else
          format.html { render action: "edit" }
          format.js { render js: "alert('Please try again');" }
        end
      end
    end

    def destroy
      @item = scoped_collection.find(params[:id])

      if @item.destroy
        redirect_to action: :index
      else
        render :show
      end
    end

    def metadata_fields
      LibraryMetadata.attribute_types
    end
    helper_method :metadata_fields

    protected

    def permitted_params
      attributes = [:blob_signed_id, library_metadata: metadata_fields.keys]
      @permitted_params ||= params.require(:item).permit(*attributes)
    end

    ##
    # Applies user search and filters to the +scoped_collection+
    def filtered_collection
      scoped_collection.keyword_search(params[:q])
    end

    ##
    # Overridable method for applying access scopes to the +base_collection+.
    # For example, assets owned by a current_user
    def scoped_collection
      base_collection
    end

    ##
    # Broadest collection of records
    def base_collection
      Item.all.includes(:blob)
    end
  end
end
