module Storehaus
  class ApplicationController < ActionController::Base
    layout 'storehaus/application'
    protect_from_forgery with: :exception
    before_action :configured_before_action

    def js_redirect_to(redirect_uri, **options)
      render options.merge(js: "window.location.replace('#{redirect_uri}');")
    end

    def configured_before_action
      action = ::Storehaus.config.before_action
      return unless action

      case action
      when Symbol, String
        public_send action
      else
        action.try(:call)
      end
    end
  end
end
