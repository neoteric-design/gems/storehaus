//= require rails-ujs
//= require activestorage
//= require ./cropper
//= require ./direct_upload
//= require ./transformations

function resetAndSubmit(form) {
  form.reset()
  Rails.fire(form, 'submit')
}