window.onload = function () {
  window.cropInputX = document.querySelector('[name="crop[x]"]')
  window.cropInputY = document.querySelector('[name="crop[y]"]')
  window.cropInputW = document.querySelector('[name="crop[w]"]')
  window.cropInputH = document.querySelector('[name="crop[h]"]')
  initCropper()
}

function maximizeCropbox () {
  const imageData = window.cropper.getImageData()
  window.cropper.setData({
    x: 0,
    y: 0,
    scale: 0,
    zoom: 1,
    width: imageData.naturalWidth,
    height: imageData.naturalHeight,
    rotate: 0
  })
}

function unlockCropbox () {
  window.cropper.setAspectRatio('NaN')
}

function rotateCropbox () {
  const cropbox = window.cropper.getCropBoxData()
  setCropperRatio(cropbox.height / cropbox.width)
}

function setCropperRatio (newRatio) {
  window.cropper.setAspectRatio(newRatio)
  return (newRatio)
}

function deliverToParent (placementData) {
  if (!window.opener) {
    console.warn('No parent window to deliver to!', placementData)
    return false
  }

  window.opener.postMessage(placementData, window.location.origin)
  window.opener.focus()
  window.close()
}

function getTransformationFields () {
  return {
    width: parseFloat(window.cropInputW.value),
    height: parseFloat(window.cropInputH.value),
    x: parseFloat(window.cropInputX.value),
    y: parseFloat(window.cropInputY.value)
  }
}

function updateTransformationFields (x, y, w, h) {
  window.cropInputX.value = x
  window.cropInputY.value = y
  window.cropInputW.value = w
  window.cropInputH.value = h
}

function initCropper () {
  var cropperImage = document.getElementById('cropper-image')

  if (cropperImage === null) { return false }
  const aspectRatio = getRatio(document.querySelector('input[name="ratio"]:checked'))

  window.cropper = new Cropper(cropperImage, {
    data: getTransformationFields(),
    aspectRatio: aspectRatio,
    viewMode: 2,
    zoomable: false,
    movable: false,
    crop: function (e) {
      updateTransformationFields(e.detail.x, e.detail.y,
        e.detail.width, e.detail.height)
    }
  })

  return true
}

function getRatio (ratioButton) {
  if (!ratioButton) {
    return 'NaN'
  }
  return ratioButton.value.split(',')
    .map(function (item) { return parseInt(item) })
    .reduce(function (acc, curr) { return (acc / curr) })
}
