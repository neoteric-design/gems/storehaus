module Storehaus
  class Item < ApplicationRecord
    paginates_per 30
    belongs_to :blob, class_name: "ActiveStorage::Blob", dependent: :destroy
    delegate :filename, to: :blob

    attribute :library_metadata, LibraryMetadata::Type.new,
              default: LibraryMetadata.new

    def blob_signed_id=(signed_id)
      self.blob = ActiveStorage::Blob.find_signed(signed_id, purpose: :blob_id)
    end

    def self.keyword_search(term)
      return all if term.blank?
      query_string = if connection.adapter_name == "PostgreSQL"
        "EXISTS(SELECT value FROM jsonb_each(library_metadata) WHERE value::varchar ILIKE :term) OR active_storage_blobs.filename ILIKE :term"
      else
        "CAST(library_metadata AS CHAR(2000)) LIKE :term OR active_storage_blobs.filename LIKE :term"
      end
      joins(:blob).where(query_string, term: "%#{term}%")
    end
  end
end
