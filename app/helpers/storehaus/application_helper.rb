module Storehaus
  module ApplicationHelper
    def thumbnail_source(attachment, **options)
      return missing_icon unless attachment
      if attachment.image?
        main_app.rails_representation_path(attachment.variant(options))
      elsif attachment.previewable?
        main_app.rails_representation_path(attachment.preview(options))
      else
        no_preview_icon
      end
    end

    def missing_icon
      'storehaus/missing.png'
    end

    def no_preview_icon
      'storehaus/no_preview.png'
    end

    def free_crop_ratios
      [[0, 0], [3, 2], [4, 3], [5, 4], [16, 9], [1, 1]]
    end

    def crop_ratios
      free_crop_ratios.push(locked_ratio).compact.uniq
    end

    def freehand_cropping?
      locked_ratio.blank?
    end

    def initial_ratio
      locked_ratio || [0, 0]
    end

    def locked_ratio 
      params[:ratio].split(',').map(&:to_i) 
    rescue
      nil
    end

    def ratio_label(ratio)
      return "Free" if ratio == [0, 0]
      ratio.join('x')
    end

    def ratio_button(ratio)
      label_tag do
        concat radio_button_tag 'ratio', ratio.join(','), ratio == initial_ratio,
                                onchange: "setCropperRatio(getRatio(this))",
                                disabled: !freehand_cropping?
        concat content_tag :span, ratio_label(ratio)
      end
    end
  end
end
