## Storehaus Changelog

# 0.7.2

* Ruby 3 fixes

# 0.7.1

* Fix cross site scripting error on cropper form

# 0.7.0

* Rails >= 6 compat
* Rails < 5.2 incompat

# 0.6.1

* Add Save/Save and Add buttons to form

# 0.6.0

* Add locked cropper aspect ratio support
