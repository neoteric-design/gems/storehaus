Storehaus::Engine.routes.draw do
  resources :items

  get 'picker' => 'items#index', as: :picker
  get 'cropper/:id' => 'utilities#cropper', as: :cropper
  get 'deliver/:id' => 'utilities#deliver', as: :deliver, defaults: { format: :js }

  root to: 'items#index'
end
