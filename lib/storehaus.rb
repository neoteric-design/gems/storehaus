# fronzen_string_literal: true

require "storehaus/engine"
require "storehaus/configuration"
require 'kaminari'

module Storehaus
  extend ActiveSupport::Autoload

  autoload :LibraryMetadata
  autoload :BlobExtensions
  autoload :AttachmentExtensions
  autoload :InlineAttachmentFinder
  autoload :InlineAttachmentSaver

  autoload :ActsAsInlineAttachable
  autoload :Transformation
end
