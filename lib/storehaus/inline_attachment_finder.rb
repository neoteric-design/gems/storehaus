# fronzen_string_literal: true

module Storehaus
  # Scan a blob of HTML and identifies the embedded uploaded images
  class InlineAttachmentFinder
    attr_reader :document, :attachments
    def initialize(content)
      @document = Nokogiri::HTML.fragment(content.to_s)
      @attachments = {}
    end

    def call
      @attachments = active_storage_images.map do |node|
        signed_id = extract_signed_id(node)
        data = { blob_id: get_blob_id(signed_id), signed_id: signed_id }
        transformation = parse_transformation(node['data-storehaus-transformation'])

        unless transformation == false
          data[:storehaus_transformation] = transformation
        end

        data
      end
    end

    private

    def active_storage_images
      document.css('img').select { |node| is_active_storage_image?(node) }
    end

    def is_active_storage_image?(node)
      node['src'].to_s.match?(active_storage_path_regex)
    end

    def extract_signed_id(node)
      node['src'].to_s.split("/")[-2]
    end

    def active_storage_path_regex
      %r{\/rails\/active_storage\/(?:blobs|representations)\/([^\/]+)\/}
    end

    def get_blob_id(signed_id)
      ActiveStorage.verifier.verify(signed_id, purpose: :blob_id)
    end

    def parse_transformation(attribute)
      if attribute.nil?
        false
      elsif attribute.blank?
        nil
      else
        JSON.parse(attribute) rescue false
      end
    end
  end
end
