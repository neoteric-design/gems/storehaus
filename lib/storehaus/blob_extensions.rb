# fronzen_string_literal: true

module Storehaus
  module BlobExtensions
    extend ActiveSupport::Concern

    included do
      scope :without_previews, lambda {
        includes(:attachments)
          .where.not(active_storage_attachments: { name: 'preview_image' })
          .or(
            ActiveStorage::Blob.includes(:attachments)
            .where(active_storage_attachments: { blob_id: nil })
          )
      }

      scope :video, -> { where("content_type LIKE 'video%'") }
      scope :image, -> { where("content_type LIKE 'image%'") }
      scope :document, -> { where("content_type LIKE 'text%' OR content_type LIKE 'application%'") }

      has_one :storehaus_item, class_name: "Storehaus::Item", dependent: :destroy
      delegate :library_metadata, :library_metadata=, to: :storehaus_item, allow_nil: true

    end
  end
end
