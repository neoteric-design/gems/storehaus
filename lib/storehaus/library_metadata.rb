# fronzen_string_literal: true

module Storehaus
  # Value object for a file's editorial metadata. with castable attributes
  class LibraryMetadata
    include ActiveModel::AttributeAssignment
    include ActiveModel::Attributes
    include ActiveModel::Serializers::JSON

    def initialize(attrs = {})
      super()
      assign_attributes attrs || {}
    end

    def ==(other)
      self.class == other.class && attributes == other.attributes
    end

    # Serialize/deserialize from database JSON field
    class Type < ActiveRecord::Type::Value
      def cast(value)
        case value
        when String
          decoded = ActiveSupport::JSON.decode(value) rescue nil
          LibraryMetadata.new(decoded)
        when Hash
          LibraryMetadata.new(value)
        else
          super
        end
      end

      def serialize(value)
        ActiveSupport::JSON.encode(value)
      end
    end
  end
end
