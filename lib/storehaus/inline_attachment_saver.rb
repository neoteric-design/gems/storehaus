# fronzen_string_literal: true

module Storehaus
  class InlineAttachmentSaver
    attr_reader :record, :found_attachments
    def initialize(record, field)
      @record = record
      @found_attachments = InlineAttachmentFinder.new(record.public_send(field)).call
      @proxy = record.public_send("#{field}_inline_files")
    end

    # Add new items to storehaus; attach new items; destroy removed items
    def call
      new_blob_ids.each { |id| Item.find_or_create_by(blob_id: id) }
      @proxy.attach(ActiveStorage::Blob.where(id: new_blob_ids))
      @proxy.attachments.select { |a| removed_blob_ids.include?(a.blob_id) }.each(&:destroy)
    end

    def found_blob_ids
      found_attachments.map { |found| found[:blob_id] }.uniq
    end

    def existing_blob_ids
      @proxy.attachments.pluck(:blob_id)
    end

    def new_blob_ids
      found_blob_ids - existing_blob_ids
    end

    def removed_blob_ids
      existing_blob_ids - found_blob_ids
    end
  end
end