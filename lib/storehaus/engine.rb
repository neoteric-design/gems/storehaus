# fronzen_string_literal: true

module Storehaus
  ##
  # Rails Engine
  class Engine < ::Rails::Engine
    isolate_namespace Storehaus

    initializer "storehaus.assets.precompile" do |app|
      app.config.assets.precompile += %w(storehaus/no_preview.png
                                         storehaus/missing.png
                                         storehaus/placeholder.png
                                         storehaus/application.css
                                         storehaus/application.js)
    end

    config.to_prepare do
      ActiveStorage::Blob.include(BlobExtensions)
      ActiveStorage::Attachment.include(AttachmentExtensions)
      ActiveRecord::Base.include(ActsAsInlineAttachable)
    end

    config.generators do |gen|
      gen.test_framework :rspec, view_specs: false
    end
  end
end
