# fronzen_string_literal: true

module Storehaus
  class Transformation
    include ActiveModel::AttributeAssignment
    include ActiveModel::Attributes
    include ActiveModel::Serializers::JSON

    attribute :crop

    def initialize(attrs = {})
      super()
      attrs ||= {}
      assign_attributes attrs.deep_symbolize_keys
    end

    def attributes
      super.deep_symbolize_keys
    end

    delegate :dig, to: :attributes

    def to_h
      attributes
    end

    def to_s
      to_json
    end

    def ==(other)
      self.class == other.class && attributes == other.attributes
    end

    def command
      @transformation = {}
      @transformation[:crop] = crop_command if crop?

      @transformation
    end

    def crop_command
      "#{crop[:w].to_i}x#{crop[:h].to_i}+#{crop[:x].to_i}+#{crop[:y].to_i}"
    end

    def crop?
      crop_keys = %i[w h x y]
      crop.present? && crop_keys.all? { |key| crop[key].present? }
    end

    # Serialize/deserialize from database JSON field
    class Type < ActiveRecord::Type::Value
      def cast(value)
        case value
        when String
          decoded = ActiveSupport::JSON.decode(value) rescue nil
          Transformation.new(decoded)
        when Hash
          Transformation.new(value)
        else
          super
        end
      end

      def serialize(value)
        ActiveSupport::JSON.encode(value)
      end
    end
  end
end