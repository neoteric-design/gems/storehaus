# fronzen_string_literal: true

module Storehaus
  module AttachmentExtensions
    extend ActiveSupport::Concern

    included do
      attribute :storehaus_transformation,
                Transformation::Type.new,
                default: Transformation.new

      def storehaus_variant(transformations = {})
        variant(transformations.with_defaults(storehaus_transformation.command))
      end

      def signed_id
        blob&.signed_id
      end

      def signed_id=(id)
        self[:blob_id] = ActiveStorage.verifier.verify(id, purpose: :blob_id)
      end

      # if defined?(RankedModel)
      #   include RankedModel
      #   ranks :sort_order, with_same: [:parent_id, :parent_type]
      #   default_scope { rank(:sort_order) }
      # end
    end
  end
end
