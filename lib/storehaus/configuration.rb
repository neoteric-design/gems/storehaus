# fronzen_string_literal: true

module Storehaus
  class << self
    attr_reader :config
    ##
    # Set configuration options
    #
    #   Storehaus.configure do |config|
    #     config.before_action = :authenticate_admin_user!
    #     config.register_metadata_field :taken_on, :datetime
    #     config.register_metadata_field :author, :string, default: "Joe"
    #   end

    def configure
      @config = Configuration.new
      yield config
    end
  end

  ##
  # Gem config storage
  class Configuration
    SETTINGS = %i[before_action].freeze
    attr_accessor(*SETTINGS)

    ##
    # Add attributes to LibraryMetadata.
    # Accepts +attribute_name+, +attribute_type+, and +options+
    # See also: http://api.rubyonrails.org/classes/ActiveRecord/Attributes/ClassMethods.html#method-i-attribute
    def register_metadata_field(*args)
      LibraryMetadata.attribute(*args)
    end
  end
end
