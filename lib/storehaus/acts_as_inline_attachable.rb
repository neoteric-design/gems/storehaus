# fronzen_string_literal: true

module Storehaus
  module ActsAsInlineAttachable
    extend ActiveSupport::Concern

    included do
    end

    class_methods do
      # Specify an attribute that may contain inline links to Blobs
      # Adds an attachment association, and a callback to update
      # the association on save.
      def inline_attachments(field, **_options)
        has_many_attached :"#{field}_inline_files", dependent: false
        before_save :"save_#{field}_inline_files", if: :"#{field}_changed?"

        define_method "save_#{field}_inline_files" do
          InlineAttachmentSaver.new(self, field).call
        end
      end
    end
  end
end
