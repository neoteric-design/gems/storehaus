module Storehaus
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)

    def copy_templates
      template "initializer.rb.erb",
               "config/initializers/storehaus.rb"
    end

    def setup_routes
      route "mount Storehaus::Engine => '/storehaus/'"
    end

    def install_migrations
      rake 'storehaus:install:migrations'
    end
  end
end
