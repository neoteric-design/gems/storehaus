class AddExtensionFieldsToActiveStorageAttachment < ActiveRecord::Migration[5.2]
  def change
    add_column :active_storage_attachments, :sort_order, :integer
    case connection.adapter_name.downcase.to_sym
    when :postgresql
      add_column :active_storage_attachments, :storehaus_transformation, :jsonb, default: {}
    else
      add_column :active_storage_attachments, :storehaus_transformation, :json
    end
  end
end
