class CreateStorehausItems < ActiveRecord::Migration[5.2]
  def change
    create_table :storehaus_items do |t|
      if t.respond_to?(:jsonb)
        t.jsonb :library_metadata, default: {}
      else
        t.json :library_metadata
      end
      t.references :blob,  null: false
      t.timestamps
    end
  end
end
