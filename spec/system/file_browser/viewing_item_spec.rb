require 'rails_helper'

RSpec.describe "File browser: Viewing item", type: :system do
  let(:item) { create(:item, library_metadata: { author: 'bob' }) }

  before :each do
    visit storehaus.item_path(item)
  end

  it "can be deleted", js: true do
    page.accept_alert 'Are you sure?' do
      click_on "Delete"
    end

    expect(current_path).to eq(storehaus.items_path)
    expect(page).to_not have_text(item.filename)
  end

  it "can be downloaded" do
    expect(page).to have_link(href: rails_blob_path(item.blob, disposition: 'attachment'))
  end

  it "displays a preview" do
    expect(page).to have_css('img')
  end

  it "metadata is listed" do
    expect(page).to have_text(item.library_metadata.author)
  end
end
