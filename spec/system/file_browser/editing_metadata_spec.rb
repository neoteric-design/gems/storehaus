require 'rails_helper'

RSpec.describe "File browser: Editing metadata", type: :system do
  let!(:item) { create :item, library_metadata: { taken_on: '2018-10-10' } }

  it "has metadata fields" do
    visit storehaus.edit_item_path(item)

    expect(page).to have_field("Taken on", with: '2018-10-10')
  end

  it "can save and add another", js: true do
    visit storehaus.edit_item_path(item)

    fill_in "Taken on", with: "2018-10-14"

    click_on "Save and Add"

    expect(page).to have_field('item[blob_signed_id]', visible: :all) # Check for something on #new form so capybara will wait
    expect(current_path).to eq(storehaus.new_item_path)
    expect(item.reload.library_metadata.taken_on).to eq(Date.parse "2018-10-14")
  end

  it "can save and deliver to parent form", js: true do
    visit storehaus.root_path
    original_window = current_window

    storehaus_window = window_opened_by do
      page.execute_script "window.open('#{storehaus.edit_item_path(item)}')"
    end

    switch_to_window storehaus_window
    fill_in "Taken on", with: "2018-10-14"
    click_on "Save"

    sleep 2

    expect(storehaus_window).to be_closed
    expect(item.reload.library_metadata.taken_on).to eq(Date.parse "2018-10-14")
  end
end
