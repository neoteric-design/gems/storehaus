require 'rails_helper'

RSpec.describe "File browser: Uploading file", type: :system, js: true do
  before :each do
    visit storehaus.new_item_path
  end

  it "creates blob via direct upload" do
    attach_file 'item[blob_signed_id]', file_fixture('test-image-a.jpg').realpath
    click_on "Upload"

    expect(page).to have_text('test-image-a.jpg')
  end

  it "after upload, goes to edit metadata" do
    attach_file 'item[blob_signed_id]', file_fixture('test-image-a.jpg').realpath
    click_on "Upload"

    expect(page).to have_field("Taken on")
  end
end
