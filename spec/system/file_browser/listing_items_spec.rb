require 'rails_helper'

RSpec.describe "File browser: Editing metadata", type: :system do
  let!(:image) { create :item, :image, library_metadata: { title: 'but' } }
  let!(:video) { create :item, :video }
  let!(:document) { create :item, :document }
  let!(:misc) { create :item, :misc }

  before :each do
    visit storehaus.items_path
  end

  it "has previews" do
    expect(page).to have_css('.item-image', count: 4)
  end

  it "can search", js: true do
    fill_in "q", with: "but"
    click_on "Search"

    expect(page).to have_css('.item-image', count: 1)
  end
end
