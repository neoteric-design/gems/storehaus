require 'rails_helper'

module Storehaus
  RSpec.describe Item, type: :model do
    let(:item) { build(:item) }
    let(:a_date) { Date.yesterday }

    it "has an instance of LibraryMetadata" do
      expect(item.library_metadata).to be_a(LibraryMetadata)
    end

    describe "Serialization" do
      it "automatically builds from json" do
        item.library_metadata = { taken_on: a_date, author: nil }.to_json
        expect(item.library_metadata).to be_a(LibraryMetadata)
        expect(item.library_metadata.taken_on).to eq(a_date)
        expect(item.library_metadata.author).to be_nil
      end
    end

    describe "blob" do
      it "can be assigned from blob_signed_id=" do
        blob = create(:blob)
        item.blob_signed_id = blob.signed_id
        expect(item.blob_id).to eq(blob.id)
      end

      it "has delegated library_metadata" do
        item.library_metadata = { author: "Joe" }
        item.save
        expect(item.blob.library_metadata).to eq(item.library_metadata)
      end

      it "removes Item when blob destroyed" do
        blob = item.blob
        blob.destroy

        expect { item.reload }.to raise_error(ActiveRecord::RecordNotFound)
      end

      it "destroyed when item destroyed" do
        item = create(:item, :video)
        blob = item.blob

        item.destroy
        expect(blob).to be_destroyed
      end
    end

    describe 'keyword_search' do
      let!(:pyramid) { create(:item, library_metadata: { title: 'Pyramid', author: "Bob" }) }
      let!(:machu) { create(:item, library_metadata: { title: 'Machu Picchu', author: "Grass Tall" }) }
      let!(:knoll) { create(:item, library_metadata: { title: 'Grassy Knoll', author: "Lush" }) }

      it 'matches a partial keyword' do
        expect(Item.keyword_search('pyra')).to match_array([pyramid])
      end

      it 'matches a keyword in metadata' do
        expect(Item.keyword_search('bob')).to match_array([pyramid])
      end

      it 'matches a keyword across title and metadata' do
        expect(Item.keyword_search('grass')).to match_array([machu, knoll])
      end
    end
  end
end
