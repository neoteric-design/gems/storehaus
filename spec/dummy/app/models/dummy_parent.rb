class DummyParent < ApplicationRecord
  has_one_attached :a_file
  has_many_attached :other_files
end
