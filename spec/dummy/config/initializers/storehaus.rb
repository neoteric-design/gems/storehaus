Storehaus.configure do |config|
  config.before_action = nil
  config.register_metadata_field :title, :string
  config.register_metadata_field :author, :string
  config.register_metadata_field :taken_on, :date
end