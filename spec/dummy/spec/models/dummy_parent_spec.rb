require 'rails_helper'

RSpec.describe DummyParent, type: :model do
  let(:item) { create(:item) }
  it "has attached files" do
    expect(subject).to respond_to(:a_file, :other_files)
  end

  it "has library_metadata for each attached file" do
    subject.a_file = item.blob
    expect(subject.a_file).to respond_to(:library_metadata)
  end
end
