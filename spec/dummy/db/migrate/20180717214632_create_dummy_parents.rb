class CreateDummyParents < ActiveRecord::Migration[5.2]
  def change
    create_table :dummy_parents do |t|
      t.text :body
      t.timestamps
    end
  end
end
