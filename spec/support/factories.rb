FactoryBot.define do
  factory :blob, class: ActiveStorage::Blob do
    transient do
      attach { 'test-image-a.jpg' }
    end

    initialize_with { ActiveStorage::Blob.create_and_upload!(io: File.new(Rails.root.join('..', 'fixtures', 'files', attach)), filename: attach) }
  end

  factory :item, class: Storehaus::Item do
    transient do
      attach { 'test-image-a.jpg' }
    end

    trait :image do
      attach { 'test-image-a.jpg' }
    end

    trait :video do
      attach { 'test-video.mov' }
    end

    trait :document do
      attach { 'test-document.rtf' }
    end

    trait :misc do
      attach { 'test-misc.zip' }
    end

    after :build do |record, evaluator|
      record.blob = build(:blob, attach: evaluator.attach)
    end
  end
end
