Capybara.server = :webrick
Capybara.server_host = '0.0.0.0'

RSpec.configure do |config|
  config.before(:each, type: :system) do
    driven_by :rack_test
  end

  if ENV["SELENIUM_URL"]
    config.before(:each, type: :system, js: true) do
      driven_by :selenium, using: :chrome, screen_size: [1400, 2000],
                options: { url: ENV["SELENIUM_URL"] }
      ip = Socket.ip_address_list.detect{ |addr| addr.ipv4_private? }.ip_address
      Capybara.app_host = "http://#{ip}:#{Capybara.server_port}"
    end
  else
    config.before(:each, type: :system, js: true) do
      driven_by :selenium_chrome_headless
    end
  end
end