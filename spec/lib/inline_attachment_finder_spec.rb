require 'rails_helper'

RSpec.describe Storehaus::InlineAttachmentFinder do
  describe "#find_signed_ids" do
    let(:blob1) { create(:blob) }
    let(:blob2) { create(:blob) }

    let :sample_body do
      <<~BODY
        <figure class=\"image\"><img src=\"/rails/active_storage/blobs/#{blob1.signed_id}/undefined\"></figure><p>&nbsp;</p><figure class=\"image\"><img src=\"/rails/active_storage/blobs/#{blob2.signed_id}/undefined\"></figure>"
      BODY
    end

    it "scans a blob of html and returns blob signed ids" do

      subject = Storehaus::InlineAttachmentFinder.new(sample_body).call

      expect(subject[0]).to eq(blob_id: blob1.id, signed_id: blob1.signed_id)
      expect(subject[1]).to eq(blob_id: blob2.id, signed_id: blob2.signed_id)
    end
  end
end
