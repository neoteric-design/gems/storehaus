require 'rails_helper'

RSpec.describe Storehaus::LibraryMetadata do
  let(:a_date) { Date.yesterday }

  describe "Attributes" do
    it "gets attributes from Configuration" do
      subject = Storehaus::LibraryMetadata.new
      expect(subject.attributes.keys).to include('taken_on', 'author')
    end

    it "initializer sets and casts values" do
      subject = Storehaus::LibraryMetadata.new('taken_on' => a_date.to_s)
      expect(subject.taken_on).to be_a(Date)
      expect(subject.taken_on).to eq(a_date)
    end

    it "has accessors" do
      subject = Storehaus::LibraryMetadata.new
      subject.author = "James"
      subject.taken_on = a_date.to_s
      expect(subject.author).to eq("James")
      expect(subject.taken_on).to eq(a_date)
    end

    it "can be equality compared" do
      one = described_class.new(taken_on: a_date.to_s, author: "Bob")
      two = described_class.new(taken_on: a_date.to_s, author: "Bob")
      three = described_class.new(author: "Bob")

      expect(one).to eq(two)
      expect(two).to_not eq(three)
    end
  end
end
