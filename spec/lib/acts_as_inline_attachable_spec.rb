require 'rails_helper'

DummyParent.class_eval do
  inline_attachments :body
end

def blob_path(blob)
  Rails.application.routes.url_helpers.rails_blob_path(blob, only_path: true)
end

RSpec.describe Storehaus::ActsAsInlineAttachable do
  let(:blob1) { create :blob, storehaus_item: build(:item, library_metadata: { author: "Foo" }) }
  let(:blob2) { create :blob }

  let(:inline_tf) do
    { crop: { x: 1, y: 2, w: 50, h: 65 } }
  end
  let(:sample_tf) do
    { crop: { x: 0, y: 10, w: 100, h: 500 } }
  end

  let :sample_body do
    <<~BODY
      <figure class="image"><img src="#{blob_path(blob1)}" data-storehaus-transformation="#{ERB::Util.html_escape(inline_tf.to_json)}"></figure><p>&nbsp;</p><figure class=\"image\"><img src=\"#{blob_path(blob2)}\"></figure><figure class="image"><img src="#{blob_path(blob1)}"></figure>
    BODY
  end

  let(:subject) { DummyParent.new(body: sample_body) }
  let(:subject_with_existing) do
    rec = DummyParent.create
    rec.body_inline_files_attachments.create(blob: blob1, storehaus_transformation: sample_tf)

    rec.body = sample_body
    rec
  end

  it "adds active storage many attachment" do
    expect(subject.body_inline_files).to be_a(ActiveStorage::Attached::Many)
  end

  it "associates blobs to parent" do
    subject.save


    expect(subject.body_inline_files.map(&:blob)).to eq([blob1, blob2])
  end

  context "blob is in storehaus" do
    it "doesn't touch item" do
      subject.save

      expect(blob1.storehaus_item.library_metadata.author).to eq("Foo")
    end
  end

  context "blob isn't yet in storehaus" do
    it "attaches a blank storehaus item" do
      subject.save

      expect(blob2.storehaus_item).to be_present
    end
  end

  context "removed" do
    let(:sample_body) { '' }

    it "remove attachments" do
      subject_with_existing.save
      subject_with_existing.body_inline_files.reload
      expect(subject_with_existing.body_inline_files.attached?).to be_falsey
    end
  end

  # describe "transformation attributes" do
  #   context "tag present" do
  #     it "saves transformation on attachment" do
  #       pending
  #       subject.save

  #       transformation = subject.body_inline_files_attachments.first.storehaus_transformation
  #       expect(transformation.crop).to eq(inline_tf[:crop])
  #     end
  #   end

  #   context "attr missing" do
  #     let :sample_body do
  #       <<~BODY
  #         <figure class="image"><img src="#{blob_path(blob1)}"></figure><p>&nbsp;</p><figure class=\"image\"><img src=\"#{blob_path(blob2)}\"></figure>
  #       BODY
  #     end

  #     it "doesn't touch existing transformation" do
  #       # is this desired behavior? tested just to know if it changes
  #       pending; fail

  #       attachment = subject_with_existing.body_inline_files_attachments.first
  #       expect(attachment.storehaus_transformation.crop).to eq(sample_tf[:crop])

  #       subject_with_existing.save

  #       expect(attachment.reload.storehaus_transformation.crop).to eq(sample_tf[:crop])
  #     end
  #   end

  #   context "attr empty" do
  #     let :sample_body do
  #       <<~BODY
  #         <figure class="image"><img src="#{blob_path(blob1)}" data-storehaus-transformation=""></figure><p>&nbsp;</p><figure class=\"image\"><img src=\"#{blob_path(blob2)}\"></figure>
  #       BODY
  #     end
  #     it "clears transformation" do
  #       pending
  #       attachment = subject_with_existing.body_inline_files_attachments.first
  #       expect(attachment.storehaus_transformation.crop).to eq(sample_tf[:crop])

  #       subject_with_existing.save

  #       expect(attachment.reload.storehaus_transformation.crop).to be_nil
  #     end
  #   end

  #   context "attr malformed data" do
  #     let :sample_body do
  #       <<~BODY
  #         <figure class="image"><img src="#{blob_path(blob1)}" data-storehaus-transformation="{s}"></figure><p>&nbsp;</p><figure class=\"image\"><img src=\"#{blob_path(blob2)}\"></figure>
  #       BODY
  #     end
  #     it "doesn't touch existing transformation" do
  #       attachment = subject_with_existing.body_inline_files_attachments.first
  #       expect(attachment.storehaus_transformation.crop).to eq(sample_tf[:crop])

  #       subject_with_existing.save

  #       expect(attachment.reload.storehaus_transformation.crop).to eq(sample_tf[:crop])
  #     end
  #   end
  # end
end
