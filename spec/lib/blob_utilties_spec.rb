require 'rails_helper'

RSpec.describe Storehaus::BlobExtensions do
  describe "::without_previews" do
    it "filters out blobs that are preview images of video/previewable blob" do
      video = create(:blob, attach: 'test-video.mov')
      image = create(:blob, attach: 'test-image-a.jpg')
      document = create(:blob, attach: 'test-document.rtf')

      video.preview(resize: '10x10>').processed

      expect(ActiveStorage::Blob.count).to eq(4)
      expect(ActiveStorage::Blob.without_previews.count).to eq(3)
    end
  end
end
