require 'rails_helper'

RSpec.describe Storehaus::Transformation do
  describe "Attributes" do
    it "initializer sets and casts values" do
      subject = Storehaus::Transformation.new('crop' => { x: 0 })
      expect(subject.crop).to eq({ x: 0 })
    end

    it "can be equality compared" do
      one = described_class.new(crop: { x: 0 })
      two = described_class.new(crop: { x: 0 })
      three = described_class.new(crop: { y: 0 })

      expect(one).to eq(two)
      expect(two).to_not eq(three)
    end
  end

  describe 'crop' do
    it 'must have the required values to apply crop' do
      subject = Storehaus::Transformation.new('crop' => { x: 0 })
      expect(subject.crop?).to be_falsey

      subject.crop = { x: 0, y: 0, w: 100, h: 100 }
      expect(subject.crop?).to be_truthy
    end
  end

  describe 'command' do
    it 'turns the transformation attributes into variant options' do
      subject.crop = { x: 0, y: 0, w: 100, h: 100 }

      expect(subject.command).to have_key(:crop)
    end
  end
end
